#ifndef _USART_H
#define _USART_H

# include "stm32f4xx.h"
# include "misc.h"
# include "stdio.h"

void USART1_Init(u32 bound);
int fputc(int ch, FILE *f);


#endif
