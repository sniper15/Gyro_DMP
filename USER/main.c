/******************************************************************************************************
*	Info: 	DMP 计算Pitch和Roll，关闭上电校准，使用水平面为绝对参考系
*	Author:	zhang
*	Data:	2016/12
*	Other:	1.DMP初始化慢，上电后保持8S稳定后才可使用
*			2.有矫正零飘功能，需启用MPU6050_OFFSET_DEBUG宏为1,记录绝对水平面偏置量,并重新写入偏置量宏			
*			3.下一版本可能增加依据内部温度芯片修正温飘
*			4.未添加HCM5883L修正YAW
*			5.动态性能较差，据说不稳定（DMP）
*			6.2016/12/4 改为LCD显示
			7.2016/12/11 修改DEBUG宏为1，改为按键选择校准
******************************************************************************************************/

#include "stm32f4xx.h"
#include "mpu6050.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h" 
#include "myusart.h"
#include "I2Cdev.h"
#include "lcd.h"
#include "key.h"
#include "stmflash.h"
#define MPU6050_OFFSET_DEBUG 1//校准

#define LCD_SHOW			 1
#define USART_SHOW			 1

#define pi             3.141592653589793238462643383279

enum
{
	NORMAL,
	DEBUG,
	SAVE,
	OTHER
};

float Pitch, Roll;

u8 col_modeflag, w_flag;

extern int16_t disk_v;
extern int disk_vflag;
extern u8 mode;

#if MPU6050_OFFSET_DEBUG
extern long a_offset[3], g_offset[3];
#endif

extern long GX_OFFSET,GY_OFFSET,GZ_OFFSET,AX_OFFSET,AY_OFFSET,AZ_OFFSET;

//flash 操作
long TEXT_Buffer[6]={0};
#define TEXT_LENTH sizeof(TEXT_Buffer)	 		  	//数组长度	
#define SIZE TEXT_LENTH/4+((TEXT_LENTH%4)?1:0)
#define FLASH_SAVE_ADDR  0X08010000

void lcd_agdebug_show(long a_offset[], long g_offset[]);	
void lcd_angle_show(void);
void lcd_diskv_show(float v);
void Normal_Mode(void);
void Debug_Mode(void);
void Save_Mode(void);
void lcd_info(void);

int main ()
{
	
	SystemCoreClockUpdate();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	delay_init(168);
	IIC_Init();
	USART1_Init(115200);
	Key_Init();
	
	#if LCD_SHOW	
	LCD_Init();
	#endif
	
	lcd_info();

	Normal_Mode();
	
	while(1)
	{
		switch(mode)
		{
			case NORMAL:
			{
				Normal_Mode();	
			}break;
			
			case DEBUG:
			{
				Debug_Mode();
			}break;
			
			case SAVE:
			{
				Save_Mode();
			}break;
		}
	}
}
void lcd_agdebug_show(long a_offset[], long g_offset[])
{
	char  lcd_msg[10][30];
	sprintf(lcd_msg[0],"A_offset:");
	sprintf(lcd_msg[1],"x:%ld  ",a_offset[0]);
	sprintf(lcd_msg[2],"y:%ld  ",a_offset[1]);
	sprintf(lcd_msg[3],"z:%ld  ",a_offset[2]);
	sprintf(lcd_msg[4],"G_offset:");
	sprintf(lcd_msg[5],"x:%ld  ",g_offset[0]);
	sprintf(lcd_msg[6],"y:%ld  ",g_offset[1]);
	sprintf(lcd_msg[7],"z:%ld  ",g_offset[2]);
	sprintf(lcd_msg[8],"You can down the KEY1 save the resaut");
	sprintf(lcd_msg[9],"to the flash");
	
	LCD_ShowString(30,170,300,12,24,(u8*)lcd_msg[0]);
	LCD_ShowString(30,220,300,12,24,(u8*)lcd_msg[1]);
	LCD_ShowString(30,270,300,12,24,(u8*)lcd_msg[2]);
	LCD_ShowString(30,320,300,12,24,(u8*)lcd_msg[3]);
	LCD_ShowString(30,370,300,12,24,(u8*)lcd_msg[4]);
	LCD_ShowString(30,420,300,12,24,(u8*)lcd_msg[5]);
	LCD_ShowString(30,470,300,12,24,(u8*)lcd_msg[6]);
	LCD_ShowString(30,520,300,12,24,(u8*)lcd_msg[7]);
	LCD_ShowString(30,570,500,12,24,(u8*)lcd_msg[8]);
	LCD_ShowString(30,620,500,12,24,(u8*)lcd_msg[9]);
}

void lcd_angle_show()
{
	char lcd_msg[10][30];
	sprintf(lcd_msg[0],"Pitch:%.2f  ",Pitch);
	sprintf(lcd_msg[1],"Roll: %.2f  ",Roll);
	LCD_ShowString(30,250,200,12,24,(u8*)lcd_msg[0]);
	LCD_ShowString(30,300,200,12,24,(u8*)lcd_msg[1]);
}

void lcd_diskv_show(float v)
{
	char lcd_msg[1][30];
	sprintf(lcd_msg[0],"The disk v :%.2f m/s   ",v);
	LCD_ShowString(30,350,300,12,24,(u8*)lcd_msg[0]);
}

void Normal_Mode()
{
	float diskv;
	if(col_modeflag == NORMAL)
	{
		LCD_Fill(30,150,530,740,WHITE);
		col_modeflag = OTHER;
		
		STMFLASH_Read(FLASH_SAVE_ADDR,(u32*)TEXT_Buffer,SIZE);
	
		GX_OFFSET = TEXT_Buffer[0];
		GY_OFFSET = TEXT_Buffer[1];
		GZ_OFFSET = TEXT_Buffer[2];
		AX_OFFSET = TEXT_Buffer[3];
		AY_OFFSET = TEXT_Buffer[4];
		AZ_OFFSET = TEXT_Buffer[5];
		
		MPU_Init();
	
		while(mpu_dmp_init())
		{
			#if LCD_SHOW
			printf("MPU6050 DMP error\r\n");
			#endif
		
			#if LCD_SHOW
			LCD_Clear(WHITE);
			LCD_ShowString(30,100,200,12,24,(u8*)"MPU6050 DMP ERROR");	
			#endif
		}
	}
	if(mpu_dmp_get_data(&Pitch,&Roll,NULL)==0)   
	{
		#if USART_SHOW
		printf("Pitch:%f Roll:%f\r\n",Pitch, Roll);
		#endif

		#if LCD_SHOW
		lcd_angle_show();
		#endif
	}
	if(disk_vflag == 1)
	{
		diskv = 0.24/disk_v*10000;
		disk_vflag = 0;
		lcd_diskv_show(diskv);
	}
	w_flag = 0;
}

void Debug_Mode()
{
	if(col_modeflag == DEBUG)
		LCD_Fill(30,150,530,740,WHITE),col_modeflag = OTHER;
	
	MPU_Init();
	
	while(mpu_dmp_init())
	{
		#if LCD_SHOW
		printf("MPU6050 DMP error\r\n");
		#endif
	
		#if LCD_SHOW
		LCD_Clear(WHITE);
		LCD_ShowString(30,100,200,12,24,(u8*)"MPU6050 DMP ERROR");	
		#endif
	}
	
	#if USART_SHOW
	printf("a_offset: x:%ld\ty:%ld\tz:%ld\t\tg_offset: x:%ld\ty:%ld\tz:%ld\r\n",a_offset[0],a_offset[1],a_offset[2],g_offset[0],g_offset[1],g_offset[2]);
	#endif
	
	#if LCD_SHOW	
	lcd_agdebug_show(a_offset,g_offset);
	#endif
	w_flag = 1;
}

void Save_Mode()
{
	if(w_flag)
	{
		TEXT_Buffer[0] = g_offset[0];
		TEXT_Buffer[1] = g_offset[1];
		TEXT_Buffer[2] = g_offset[2];
		TEXT_Buffer[3] = a_offset[0];
		TEXT_Buffer[4] = a_offset[1];
		TEXT_Buffer[5] = a_offset[2];
		
		STMFLASH_Write(FLASH_SAVE_ADDR,(u32*)TEXT_Buffer,SIZE);
		LCD_ShowString(30,670,500,12,24,(u8*)"Save finsh");
		mode = NORMAL;
		col_modeflag = NORMAL;
	}
	else
	{
		LCD_ShowString(30,740,500,12,24,(u8*)"No data");
	}
}

void lcd_info()
{
	LCD_ShowString(30,0,500,12,24,(u8*)"Please wait 8s after power up");
	LCD_ShowString(30,40,500,12,24,(u8*)"You can down the KEYx to select mode:");
	LCD_ShowString(30,80,500,12,24,(u8*)"KEYup:  DEBUG  KEY1 :  SAVE");
	LCD_ShowString(30,120,500,12,24,(u8*)"KEY2 :  NORMAL");
}