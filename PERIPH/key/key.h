#ifndef _KEY_H
#define _KEY_H

#include "stm32f4xx.h"
#include "delay.h"

void Key_Init(void);
u8 Key_Scan(u8 key_val);

#endif