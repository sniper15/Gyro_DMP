# include "MPU6050.h"
# include "sys.h"
# include "myusart.h"

u8 MPU_Init(void)
{
	//IIC_Init();
	delay_ms(800);																//上电稳定
	MPU_Write_Byte(MPU_PWR_MGMT1_REG,0X80);					//复位
	delay_ms(200);
	MPU_Write_Byte(MPU_PWR_MGMT1_REG,0X00);					//设置PLL为时钟源
	MPU_Write_Byte(MPU_PWR_MGMT2_REG,0X00);					//开启加速度，陀螺仪
	MPU_Write_Byte(MPU_CFG_REG,0X06); 						//低通滤波器典型值 5Hz 陀螺仪，加速度计输出频率 = 1KHz
	MPU_Write_Byte(MPU_SAMPLE_RATE_REG,0X07);				//采样频率1kHz
	MPU_Write_Byte(MPU_GYRO_CFG_REG,0X00);  				//陀螺仪 		250 deg/s  131 LSB/deg/s     
	MPU_Write_Byte(MPU_ACCEL_CFG_REG,0X00);  				//加速度计  2g  			  16384 LSB/g
	MPU_Write_Byte(MPU_FIFO_EN_REG,0x00);					//关闭FIFO
	MPU_Write_Byte(MPU_INT_EN_REG,0X00);					//关闭所有中断
	MPU_Write_Byte(MPU_USER_CTRL_REG,0X00);					//I2C主模式关闭，HCM588L由主总线驱动
	//MPU_Write_Byte(MPU_INTBP_CFG_REG,0X02);					//允许主处理器直接访问HCM588L（辅助IIC）   暂不启用
	return 0;
}

//温度
short MPU_Get_Temperature(void)
{
    u8 buf[2]; 
    short raw;
	float temp;
	MPU_Read_Len(MPU_ADDR,MPU_TEMP_OUTH_REG,2,buf); 
    raw=((u16)buf[0]<<8)|buf[1];  
    temp=36.53+((double)raw)/340;  
    return temp*100;;
}

//得到陀螺仪值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
u8 MPU_Get_Gyroscope(short *gx,short *gy,short *gz)
{
    u8 buf[6],res;  
	res=MPU_Read_Len(MPU_ADDR,MPU_GYRO_XOUTH_REG,6,buf);
	if(res==0)
	{
		*gx=(((u16)buf[0]<<8)|buf[1]);;  
		*gy=(((u16)buf[2]<<8)|buf[3]);;  
		*gz=(((u16)buf[4]<<8)|buf[5]);;
	} 	
    return res;;
}
//得到加速度值(原始值)
//ax,ay,az加速度x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
u8 MPU_Get_Accelerometer(short *ax,short *ay,short *az)
{

    u8 buf[6],res;  
	res=MPU_Read_Len(MPU_ADDR,MPU_ACCEL_XOUTH_REG,6,buf);

	if(res==0)
	{
		*ax=((u16)buf[0]<<8)|buf[1];  
		*ay=((u16)buf[2]<<8)|buf[3];  
		*az=((u16)buf[4]<<8)|buf[5];
	} 	
    return res;;
}
//IIC连续写
//addr:器件地址 
//reg:寄存器地址
//len:写入长度
//buf:数据区
//返回值:0,正常
//    其他,错误代码
u8 MPU_Write_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{
	u8 i; 
    IIC_Start(); 
	IIC_Send_Byte((addr<<1)|0);
	if(IIC_Wait_Ack())	
	{
		IIC_Stop();		 
		return 1;		
	}
    IIC_Send_Byte(reg);	
    IIC_Wait_Ack();		
	for(i=0;i<len;i++)
	{
		IIC_Send_Byte(buf[i]);	
		if(IIC_Wait_Ack())		
		{
			IIC_Stop();	 
			return 1;		 
		}		
	}    
    IIC_Stop();	 
	return 0;	
} 
//IIC连续读
//addr:器件地址
//reg:要读取的寄存器地址
//len:要读取的长度
//buf:读取到的数据存储区
//返回值:0,正常
//    其他,错误代码
u8 MPU_Read_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{ 
 	IIC_Start(); 
	IIC_Send_Byte((addr<<1)|0);
	if(IIC_Wait_Ack())	
	{
		IIC_Stop();		 
		return 1;		
	}
    IIC_Send_Byte(reg);	
    IIC_Wait_Ack();		
    IIC_Start();
	IIC_Send_Byte((addr<<1)|1);
    IIC_Wait_Ack();		
	while(len)
	{
		if(len==1)*buf=IIC_Read_Byte(0);
		else *buf=IIC_Read_Byte(1);		
		len--;
		buf++; 
	}    
    IIC_Stop();	
	return 0;	
}
//IIC写一个字节 
//reg:寄存器地址
//data:数据
//返回值:0,正常
//    其他,错误代码
u8 MPU_Write_Byte(u8 reg,u8 data) 				 
{ 
    IIC_Start(); 
	IIC_Send_Byte((MPU_ADDR<<1)|0);
	if(IIC_Wait_Ack())	
	{
		IIC_Stop();		 
		return 1;		
	}
    IIC_Send_Byte(reg);	
    IIC_Wait_Ack();		
	IIC_Send_Byte(data);
	if(IIC_Wait_Ack())	
	{
		IIC_Stop();	 
		return 1;		 
	}		 
    IIC_Stop();	 
	return 0;
}
//IIC读一个字节 
//reg:寄存器地址 
//返回值:读到的数据
u8 MPU_Read_Byte(u8 reg)
{
	u8 res;
    IIC_Start(); 
	IIC_Send_Byte((MPU_ADDR<<1)|0);	
	IIC_Wait_Ack();		
    IIC_Send_Byte(reg);	
    IIC_Wait_Ack();		
    IIC_Start();
	IIC_Send_Byte((MPU_ADDR<<1)|1);
    IIC_Wait_Ack();		
	res=IIC_Read_Byte(0);
    IIC_Stop();			
	return res;		
}

void MPU6050_OFFSET(short *a_offset, short *g_offset)
{
	static int32_t ax_offset, ay_offset, az_offset,gx_offset, gy_offset, gz_offset;
	static short num;
	short ax, ay, az, gx, gy, gz;
	
	while(1)
	{
		MPU_Get_Accelerometer(&ax, &ay, &az);
		MPU_Get_Gyroscope(&gx, &gy, &gz);
		
		ax_offset += ax;
		ay_offset += ay;
		az_offset += az;
		gx_offset += gx;
		gy_offset += gy;
		gz_offset += gz;
		num++;
		
		if(num >= 1000)
		{
			a_offset[0] = ax_offset*1.0/num;
			a_offset[1] = ay_offset*1.0/num;
			a_offset[2] = az_offset*1.0/num;
			g_offset[0] = gx_offset*1.0/num;
			g_offset[1] = gy_offset*1.0/num;
			g_offset[2] = gz_offset*1.0/num;
			
			ax_offset=0;
			ay_offset=0;
			az_offset=0;
			gx_offset=0;
			gy_offset=0;
			gz_offset=0;
			num = 0;
			
			break;
		}
		delay_ms(5);
	}
	return ;
}

